// lib/pipeline-stack.ts

import codebuild = require('@aws-cdk/aws-codebuild');
import codecommit = require('@aws-cdk/aws-codecommit');
import codepipeline = require('@aws-cdk/aws-codepipeline');
import codepipeline_actions = require('@aws-cdk/aws-codepipeline-actions');
import lambda = require('@aws-cdk/aws-lambda');
import s3 = require('@aws-cdk/aws-s3');
import { App, Stack, StackProps } from '@aws-cdk/core';
import { Constants } from './../../fw-corelib/lib/constants';
import { Category, Owner, Provider } from './../../fw-corelib/lib/configuration';
import { FwStageSourceS3Stack } from './../../fw-stage-source-s3/lib/fw-stage-source-s3-stack';

export interface PipelineStackProps extends StackProps {
  readonly lambdaCode: lambda.CfnParametersCode;
}

export class PipelineStack extends Stack {
  constructor(app: App, id: string, props?: PipelineStackProps | any) {
    super(app, id, props);

    

    new codepipeline.CfnPipeline(this, 'Pipeline', {
      name: 'myPipeline',
      roleArn: 'sdf',
      artifactStore: {
        location: '',
        type: ''
      },
      stages: [
        new FwStageSourceS3Stack(this, 'FwStageSourceS3Stack').getConfig()
      ],
    })
  }
}